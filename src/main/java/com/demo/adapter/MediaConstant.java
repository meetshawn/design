package com.demo.adapter;
/**
 * @author shawn
 * @since 2021/4/16 17:33
 */
public class MediaConstant {
    public static final String VLC = "vlc";
    public static final String MP4 = "mp4";
    public static final String MP3 = "mp3";
}
