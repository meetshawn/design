package com.demo.adapter;
/**
 * @author shawn
 * @since 2021/4/16 17:32
 */
public interface AdvancedMediaPlayer {
   /**
    * 播放vlc
    * @param fileName 文件名称
    */
   public void playVlc(String fileName);
   /**
    * 播放MP4
    * @param fileName 文件名称
    */
   public void playMp4(String fileName);
}