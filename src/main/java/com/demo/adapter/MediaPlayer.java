package com.demo.adapter;

/**
 * @author shawn
 * @since 2021/4/16 17:27
 */
public interface MediaPlayer {
    /**
     * 创建播放器功能
     *
     * @param audioType 类型
     * @param fileName  文件名称
     */
    public void play(String audioType, String fileName);
}