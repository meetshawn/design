package com.demo.adapter;

/**
 * mp4 播放器
 *
 * @author shawn
 * @since 2021/4/16 17:30
 */
public class Mp4Player implements AdvancedMediaPlayer {

    @Override
    public void playVlc(String fileName) {
        //什么也不做
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Playing mp4 file. Name: " + fileName);
    }
}