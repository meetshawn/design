package com.demo.adapter;

/**
 * @author shawn
 * @since 2021/4/16 17:27
 */
public class MediaAdapter implements MediaPlayer {


    AdvancedMediaPlayer advancedMusicPlayer;

    public MediaAdapter(String audioType) {
        if (MediaConstant.VLC.equalsIgnoreCase(audioType)) {
            advancedMusicPlayer = new VlcPlayer();
        } else if (MediaConstant.MP4.equalsIgnoreCase(audioType)) {
            advancedMusicPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {
        if (MediaConstant.VLC.equalsIgnoreCase(audioType)) {
            advancedMusicPlayer.playVlc(fileName);
        } else if (MediaConstant.MP4.equalsIgnoreCase(audioType)) {
            advancedMusicPlayer.playMp4(fileName);
        }
    }
}