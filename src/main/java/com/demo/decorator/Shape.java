package com.demo.decorator;

public interface Shape {
   void draw();
}