package com.demo.iterator;

public interface Iterator {
   public boolean hasNext();
   public Object next();
}