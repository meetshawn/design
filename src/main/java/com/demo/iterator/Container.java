package com.demo.iterator;

public interface Container {
   public Iterator getIterator();
}