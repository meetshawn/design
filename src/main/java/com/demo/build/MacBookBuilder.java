package com.demo.build;

/**
 * @author shawn
 * @since 2021/4/16 17:51
 */
public class MacBookBuilder extends Builder {

    /**
     * 构建一个MacBook
     */
    private final Computer mComputer = new MacBook();

    @Override
    void buildBoard(String board) {
        mComputer.setBoard(board);
    }

    @Override
    void buildDisplay(String display) {
        mComputer.setDisplay(display);
    }

    @Override
    void buildOs() {
        mComputer.setOs();
    }

    @Override
    Computer build() {
        return mComputer;
    }
}