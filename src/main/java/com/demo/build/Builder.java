package com.demo.build;

/**
 * 建造者
 *
 * @author shawn
 * @since 2021/4/16 17:45
 */
public abstract class Builder {
    /**
     * 主板
     * @param board 主板
     */
    abstract void buildBoard(String board);
    /**
     * 显示器
     * @param display 显示器
     */
    abstract void buildDisplay(String display);

    /**
     * 内存
     */
    abstract void buildOs();

    /**
     * 构建一个电脑
     * @return 电脑
     */
    abstract Computer build();

}