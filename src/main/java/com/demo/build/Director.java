package com.demo.build;

/**
 * @author shawn
 * @since 2021/4/16 17:49
 */
public class Director {
    /**
     * 建造者
     */
    Builder mBuilder = null;


    public Director(Builder builer) {
        this.mBuilder = builer;
    }


    public void construct(String board, String display) {
        mBuilder.buildDisplay(display);
        mBuilder.buildBoard(board);
        mBuilder.buildOs();
    }
}