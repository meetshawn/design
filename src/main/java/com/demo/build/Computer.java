package com.demo.build;

/**
 * @author shawn
 * @since 2021/4/16 17:44
 */
public abstract class Computer {
    protected String mBoard;
    protected String mDisplay;
    protected String mOs;


    protected Computer() {
    }


    public void setBoard(String board) {
        mBoard = board;
    }

    public void setDisplay(String display) {
        this.mDisplay = display;
    }

    /**
     * 操作系统
     */
    public abstract void setOs();


    @Override
    public String toString() {
        return "Computer{" +
                "mBoard='" + mBoard + '\'' +
                ", mDisplay='" + mDisplay + '\'' +
                ", mOs='" + mOs + '\'' +
                '}';
    }
}