package com.demo.proxy;

public interface Image {
   void display();
}